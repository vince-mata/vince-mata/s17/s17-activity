console.log("drink html")
console.log("eat javascript")
console.log("inhale css")
console.log("bake bootstrap")

let tasks=["drink html", "eat javascript", "inhale css", "bake bootstrap"]
console.log(tasks)

//to display first element
console.log(tasks[0])

//to display the last element
let indexOfLastElement = tasks.length -1
console.log(tasks[indexOfLastElement])

let indexOfMiddleElement = tasks.length -2
console.log(tasks[indexOfMiddleElement])


//Array Manipulation

let numbers = ["one", "two", "three", "four"]
console.log(numbers)


numbers.push("five")
console.log(numbers)

function pushMethod(element){
	numbers.push(element)
}

pushMethod("six")
pushMethod("seven")
pushMethod("eight")
console.log(numbers)


pushMethod("CrushAkoNgCrushko")
console.log(numbers)

	numbers.pop()
	console.log(numbers)

	function popMethod(){
	numbers.pop()
	}	
	popMethod()
	console.log(numbers)

	// ----------------------------
	//adding an element at the beginning of an array
	//UNSHIFT Method

	numbers.unshift("zero")
	console.log(numbers)

	function unshiftMethod(element){
		numbers.unshift(element)
	}
	unshiftMethod("jm pogi")
	console.log(numbers)

	//SHIFT - Delete beginning of an array
	//POP - Delete at the end of an array

	numbers.shift()
	console.log(numbers)

	function shiftMethod(){
		numbers.shift()
	}
	shiftMethod()
	console.log(numbers)


//	------------------
// ascending order
let numbs = [15,10,31,24,30]
	numbs.sort(
		function (a, b){
			return a - b
		}
	)
	console.log(numbs)

//descending order

	numbs.sort(
		function (a, b){
			return b - a
		}
	)
	console.log(numbs)

//REVERSE
	//Reverses the order of the elements of the last array

	// numbs.reverse()
	// console.log(numbs)

//Splice Method
//third element determines 


// let nums = numbs.splice(1)
// console.log(numbs)
// console.log(nums)

// let nums = numbs.splice(1)
// console.log(numbs)
// console.log(nums)

let nums = numbs.splice(3, 1, 11, 9, 1)
console.log(numbs)
console.log(nums)

//Slice Method
	//it cannot manipulate the original array
	//first paremeter - the index where the copying will start, going to the right
	//second parameter - the number of elements to be copied (counting starts at the first element, not the index)


let slicedNumbs = numbs.slice(1, 6)
console.log(slicedNumbs)
console.log(numbs)


	//Concat
	//merge two or more arrays

	console.log(numbers)
	console.log(numbs)
	let animals = [ "snake", "chimp", "dog", "cat" ]
	console.log(animals)

	let newConcat = numbers.concat(numbs,animals)
	console.log(newConcat)

	let animals2 = [ "bird", "dinosaur"]
	let concat2 = newConcat.concat(animals2)
	console.log(concat2)


// Using sort to arrange the elements inside
	// concat2.sort(
	// 	function (a, b){
	// 		return a - b
	// 	}
	// )
	// console.log(concat2)



//Join Method
	//merges the elements inside the array
	//No parameter - transforms the data type of the array into strings, seperated by comma
	//paremeters - determines the means of seperating the elements

	let meal = [ "rice", "lechon kawali", "coke" ]
	console.log(meal)

	let newJoin = meal.join()
	console.log(newJoin)

	newJoin = meal.join("")
	console.log(newJoin)

	newJoin = meal.join(" ")
	console.log(newJoin)

	newJoin = meal.join("-")
	console.log(newJoin)

//Accessors
	
	let countries = ["PKT", "US", "PH", "NZ", "UK", "AU", "SG", "JPN", "CA", "SK", "PH"]
	console.log(countries.indexOf("PH")) //2
	//determines the index of the specified element
	console.log(countries.indexOf("RU")) //-1
	//if element is non-existing it will return -1

	//lastIndexOf
		//starts the searching of the element from the end of the array
	console.log(countries.lastIndexOf("PH")) //10

	//Re-assignement of the elements and index
	countries[0] = "NK"
	console.log(countries)

	newCountries = countries.join(" ")
	console.log(newCountries)

	//Iterators
	//forEach (cb())

	let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]
	days.forEach(
		function(element){
			console.log(element)
		}
	)

	//Map method
	let mapDays = days.map(
 		function(day){
 			return `${day} is the of the week`
 		}
 	)
 	console.log(mapDays)
	console.log(days)

//Mini Activity
//Using forEach method, add each element of days array in an empty array
	
	let days2 = [];
	console.log(days2)

	days.forEach(
	function(day){
		days2.push(day)
	})
	console.log(days2)



//every() method
/*
	- it checks if all elements in an array meet the given condition
	Syntax: 
		let/const resultArray = arrayName.every(
			function(indivElement) {
				return expression/condition
			}
		)
*/

let numbersA = [1, 2, 3, 4, 5]

let allValid = numbersA.every( 
		function(number) {
			return (number < 3);
		}
	)

console.log(allValid)

//some() method
/*
	-checks if at least one element in the array meets the given condition
	-this will return a true value if at least one elements meet the condition and false if not

	Syntax: 
		let/const resultArray = arrayName.some(
			function(indivElement) {
				return expression/condition
			}
		)

*/

let someValid = numbersA.some(
		function(number) {
			return (number < 2)
		}
	)
console.log(someValid)


// filter() method 
/*
	-Returns a new array that contains elements which meets the given condition.
	-Return an empty array if no elements were found

		Syntax: 
		let/const resultArray = arrayName.filter(
			function(indivElement) {
				return expression/condition
			}
		)

*/

let filterValid = numbersA.filter(
		function(number) {
			return (number < 3)
		}
	)

console.log(filterValid)

let nothingFound = numbersA.filter(
		function(number) {
			return (number == 0)
		}
	)

console.log(nothingFound)

let numbersArray = [500, 12, 120, 60, 6, 8]

let divisibleBy5 = numbersArray.filter(
		function(number) {
			return number % 5 === 0
		}
	)
console.log(divisibleBy5)


//Filtering using ForEach

let filteredNumbers = [];

numbersA.forEach(
		function(number) {

			if(number < 3) {
				filteredNumbers.push(number)
			}
		}
	)

console.log(filteredNumbers)

//includes method

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor', 'All'];

let filteredProducts = products.filter(
			function(product) {
				return product.toLowerCase().includes('a');
			}
	)

console.log(filteredProducts)

//Multidimensional Arrays
/*
	-Multidimensional arays are useful for storing complex data structures

*/

let chessBoard = [
		['a1','b1', 'c1', 'd1', 'e1', 'f1','g1', 'h1'],
		['a2','b2', 'c2', 'd2', 'e2', 'f2','g2', 'h2'],
		['a3','b3', 'c3', 'd3', 'e3', 'f3','g3', 'h3'],
		['a4','b4', 'c4', 'd4', 'e4', 'f4','g4', 'h4'],
		['a5','b5', 'c5', 'd5', 'e5', 'f5','g5', 'h5'],
		['a6','b6', 'c6', 'd6', 'e6', 'f6','g6', 'h6'],
		['a7','b7', 'c7', 'd7', 'e7', 'f7','g7', 'h7'],
		['a8','b8', 'c8', 'd8', 'e8', 'f8','g8', 'h8'],
];

console.log(chessBoard)
console.log(chessBoard[1][4]); //e2
console.log("Pawn moves to: " + chessBoard[7][4]) //e8

	